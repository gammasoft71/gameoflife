﻿using System;

namespace GameOfLive {
  enum Cell {
    Alive,
    Dead
  }

  class Rle {
    public string Name { get; set; }
    public string Author { get; set; }
    public System.Collections.Generic.List<string> Comments { 
      get {
        return this.comments; 
      }
      set {
        this.comments = value; 
      }
    }

    System.Collections.Generic.List<string> comments = new System.Collections.Generic.List<string>();
  }

  class Grid {
    public Grid(int size) {
      this.cells = new Cell[size, size];
      this.Clear();
    }

    public Cell this[int x, int y] {
      get { return this.cells[x, y]; }
      set { this.cells[x, y] = value; }
    }

    public void Clear() {
      for (int y = 0; y < this.cells.GetLength(0); y++) {
        for (int x = 0; x < this.cells.GetLength(1); x++) {
          this.cells[x, y] = Cell.Dead;
        }
      }
    }

    public void Evolve() {
      Cell[,] currentCells = (Cell[,])this.cells.Clone();

      for (int y = 0; y < currentCells.GetLength(1); y++) {
        for (int x = 0; x < currentCells.GetLength(0); x++) {
          int neighbors = 0;

          if ((x - 1) >= 0 && currentCells[x - 1, y] == Cell.Alive)
            neighbors++;
          if ((x + 1) < currentCells.GetLength(0) && currentCells[x + 1, y] == Cell.Alive)
            neighbors++;

          if ((y - 1) >= 0) {
            if ((x - 1) >= 0 && currentCells[x - 1, y - 1] == Cell.Alive)
              neighbors++;
            if (currentCells[x, y - 1] == Cell.Alive)
              neighbors++;
            if ((x + 1) < currentCells.GetLength(0) && currentCells[x + 1, y - 1] == Cell.Alive)
              neighbors++;
          }

          if ((y + 1) < currentCells.GetLength(1)) {
            if ((x - 1) >= 0 && currentCells[x - 1, y + 1] == Cell.Alive)
              neighbors++;
            if (currentCells[x, y + 1] == Cell.Alive)
              neighbors++;
            if ((x + 1) < currentCells.GetLength(0) && currentCells[x + 1, y + 1] == Cell.Alive)
              neighbors++;
          }
          if (currentCells[x, y] == Cell.Alive && (neighbors <= 1 || neighbors >= 4))
            this.cells[x, y] = Cell.Dead;

          if (currentCells[x, y] == Cell.Alive && (neighbors == 2 || neighbors == 3))
            this.cells[x, y] = Cell.Alive;

          if (currentCells[x, y] == Cell.Dead && neighbors == 3)
            this.cells[x, y] = Cell.Alive;
        }
      }
    }

    private Cell[,] cells;
  }

  class Simulation {
    public static int MaxSpeed = 15;
    public Simulation() {
      this.run = new System.Threading.Timer(new System.Threading.TimerCallback(delegate {
        this.Evolve();
      }));
    }

    public Cell this[int x, int y] {
      get { return this.grid[x, y]; }
      set { this.grid[x, y] = value; }
    }

    public int Iteration {
      get { return this.iteration; }
    }

    public bool Running {
      get { return this.running; }
    }

    public int Speed {
      get { return this.speed; }
      set {
        if (this.speed != value) {
          if (value < 0 || value > MaxSpeed)
            throw new System.ArgumentOutOfRangeException();
          this.speed = value;
          if (this.running) {
            this.run.Change(SpeedToTimeSpan(this.speed), SpeedToTimeSpan(this.speed));
          }
        }
      }
    }

    public void Clear() {
      this.grid.Clear();
      this.iteration = 0;
    }

    public Rle LoadFromRle(string path) {
      string[] lines = System.IO.File.ReadAllLines(path);
      Rle rle = new Rle();
      foreach (string line in lines) {
        if (line.StartsWith("#N "))
          rle.Name = line.Substring(3);
        if (line.StartsWith("#O "))
          rle.Author = line.Substring(3);
        if (line.StartsWith("#C "))
          rle.Comments.Add(line.Substring(3));
      }
      return rle;
    }

    public void Generate() {
      System.Random rand = new Random();

      int max = rand.Next(10, 100);
      for (int counter = 0; counter < max; counter++) {
        this.grid[rand.Next(0, 59), rand.Next(0, 59)] = Cell.Alive; 
      }
    }

    public void Next() {
      this.Evolve();
    }

    public void Start() {
      if (!this.running) {
        this.running = true;
        this.run.Change(SpeedToTimeSpan(this.speed), SpeedToTimeSpan(this.speed));
      }
    }

    public void Stop() {
      if (this.running) {
        this.running = false;
        this.run.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
      }
    }

    private static System.TimeSpan SpeedToTimeSpan(int speed) {
      return System.TimeSpan.FromMilliseconds(1000.0 / speed);
    }

    private void Evolve() {
      this.iteration++;
      this.grid.Evolve();
    }

    private Grid grid = new Grid(1000);
    int iteration = 0;
    int speed = 1;
    bool running = false;
    System.Threading.Timer run;
  }

  class MainClass {
    public static void Main(string[] args) {
      System.Console.OutputEncoding = System.Text.Encoding.UTF8;
      bool running = true;
      Simulation simulation = new Simulation();

      //Rle rle = simulation.LoadFromRle("/Users/yves/Desktop/Pulsar.rle");

      //Console.WriteLine("Name = {0}", rle.Name);
      //Console.WriteLine("Author = {0}", rle.Author);
      //foreach(string comment in rle.Comments) {
      //  Console.WriteLine("Comment = {0}", comment);
      //}
      //return;

      int currentX = 0;
      int currentY = 0;

      System.Console.Clear();
      System.Console.CursorVisible = false;

      System.Console.BackgroundColor = ConsoleColor.Black;
      System.Console.SetCursorPosition(0, 0);
      System.Console.Write(new string(' ', 60));
      System.Console.SetCursorPosition(0, 61);
      System.Console.Write(new string(' ', 60));

      while(running) {
        if (System.Console.KeyAvailable) {
          System.ConsoleKeyInfo key = System.Console.ReadKey(true);
          switch(key.Key) {
            case ConsoleKey.Escape : running = false; break;
            case ConsoleKey.Enter: if (simulation.Running) simulation.Stop(); else simulation.Start(); break;
            case ConsoleKey.Spacebar: if (!simulation.Running) if (simulation[currentX, currentY] == Cell.Alive) simulation[currentX, currentY] = Cell.Dead; else simulation[currentX, currentY] = Cell.Alive; break;
            case ConsoleKey.UpArrow: if (!simulation.Running) if (currentY > 0) currentY--; else currentY = 59; break;
            case ConsoleKey.DownArrow: if (!simulation.Running) if (currentY < 59) currentY++; else currentY = 0; break;
            case ConsoleKey.LeftArrow: if (!simulation.Running) if (currentX > 0) currentX--; else currentX = 59; break;
            case ConsoleKey.RightArrow: if (!simulation.Running) if (currentX < 59) currentX++; else currentX = 0; break;
            case ConsoleKey.C: if (!simulation.Running) simulation.Clear(); break;
            case ConsoleKey.G: if (!simulation.Running) simulation.Generate(); break;
            case ConsoleKey.N: if (!simulation.Running) simulation.Next(); break;
            case ConsoleKey.P: if (simulation.Speed < Simulation.MaxSpeed) simulation.Speed++; break;
            case ConsoleKey.M: if (simulation.Speed > 1) simulation.Speed--; break;
          }          
        }


        System.Console.BackgroundColor = ConsoleColor.Black;
        for (int y = 0; y < 60; y++) {
          for (int x = 0; x < 60; x++) {
            if (!simulation.Running && simulation[x, y] == Cell.Alive && x == currentX && y == currentY)
              System.Console.ForegroundColor = ConsoleColor.DarkGray;
            else if (simulation[x, y] == Cell.Alive)
              System.Console.ForegroundColor = ConsoleColor.Black;
            else if (!simulation.Running && simulation[x, y] == Cell.Dead && x == currentX && y == currentY)
              System.Console.ForegroundColor = ConsoleColor.Gray;
            else if (simulation[x, y] == Cell.Dead)
              System.Console.ForegroundColor = ConsoleColor.White;
            System.Console.SetCursorPosition(x, y + 1);
            System.Console.Write('▉');
          }       
        }

        System.Console.BackgroundColor = ConsoleColor.Black;
        System.Console.SetCursorPosition(17, 0);
        System.Console.Write("John Conway's Game of Life");
        System.Console.SetCursorPosition(0, 61);
        System.Console.Write("Iteration {0,6} - Speed {1,3}", simulation.Iteration, simulation.Speed);
        //System.Threading.Thread.Sleep(10);
      }

      Console.ResetColor();
      Console.Clear();
      System.Console.CursorVisible = true;
    }
  }
}
